============
Using Sphinx
============

Here we use Sphinx to do a test build our documentation in HTML.

Example
=======

Running Sphinx
--------------

.. raw:: html

    <script type="text/javascript" src="https://asciinema.org/a/88549.js" id="asciicast-88549" async></script>

Result
------

.. image:: ../_static/using_sphinx.png
   :alt: Picture of HTML build, using Sphinx in a browser
