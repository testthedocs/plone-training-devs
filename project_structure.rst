=================
Project Structure
=================

Exercise
========

**Do we like what we see ?**

.. image:: _static/ps_cmf_plone.png
   :alt: Picture of CMFPlone /docs repo, to show what is wrong there

**Do we like that one ?**

.. image:: _static/ps_plone-barceloneta.png
   :alt: Picture of barceloneta /docs repo, to show what is wrong there

**Well, what is with that one?**

.. image:: _static/ps_ploneporg.core.png
   :alt: Picture of ploneorg.core /docs to show what is wrong there

Explanation
===========

The */docs* directory should contain only content related to documentation.

- Please **do not** put the license here. A LICENSE.rst with a short description of the license, and LICENSE.GPL for the legalese should go into the top level of your package next to your README.rst
- Please **do not** put a CHANGES, CHANGELOG, HISTORY or CONTRIBUTORS file here either. These files belongs into the top level, too.

Example
-------

.. image:: _static/ps_ok.png
   :alt: Picture of tree output, showing how a good project structure should look like
