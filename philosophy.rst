========================
Documentation Philosophy
========================

.. topic:: Definition

    Documentation is the information that describes the product to its users.
    It consists of the product technical manuals and online information (including online versions of the technical manuals and help facility descriptions).

- Think about the process of writing the documentation for your project as the true act of creation
- Writing documentation is absolutely essential to writing good software.
- Until you've written about your software, you have no idea what you'll be coding.
- Developer documentation is **not** End-user documentation.

**Documentation is equal to code**

Types Of Documentation
=======================

It’s important to distinguish **Readme Driven Development** [:term:`RDD`]  from **Documentation Driven Development** [:term:`DDD`].

RDD is a subset or limited version of DDD.

Readme Driven Development
==========================

All documentation in *one* single file

- May work for small projects
- In general more developer focused
- People tend to add build badges [Travis]

For almost any non-trivial project, it is better to think of the Readme as the back cover text of a book.
You flip through it, while standing in a book store, deciding if you should buy this or the other one.
It should be short, give you an overview of the content, and any prizes the book may have won.
It is 'advertising' for an interested audience, but **not** documentation.

Documentation Driven Development
================================

Splitting documentation into chapters

- Small and modularized
- Easier to rearrange
- Better structure, easier to read
- You can address different audiences in different styles
- Cleaner
- Most people read documentation in a non-linear way; they will read the chapter that helps them at their task, when they need it.


