=======
Testing
=======

.. toctree::
   :hidden:
   :maxdepth: 2

   test_yourself
   test_against_audience
   testing_strategy
   testing_tools
   basic_testing
   look_and_feel
   using_sphinx
   using_docker
   testing_with_linter
   testing_with_githooks
   testing_ci

.. topic:: Testing Documentation is hard

   The key to writing good documentation: Testing your instructions !

* Writing good documentation requires you to set up a test environment and test all of your instructions -- testing the instructions yourself and against a user.
* Testing instructions can be time consuming and tricky, especially with developer documentation.
* It's hard to see past personal blind spots and assumptions.
* But testing instructions gives you access to insight that makes your documentation much more accurate and useful.

.. topic:: Rule Of Thumb

    Never say “Oh, you just do a, b, and c.” It’s much more complicated than you think.
