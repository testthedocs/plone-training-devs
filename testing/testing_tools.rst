=============
Testing Tools
=============

There are different levels of testing:

- Basic testing [:term:`HIT`, Editor, Google Analytics]
- Visual testing [Editor]
- Testing with a build tool [Sphinx, ...]
- Combine tests using, scripts and/or other applications [git-hooks, CI, Docker]

