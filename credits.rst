=======
Credits
=======

Some parts of this training are taken form other talks, trainings or presentations !

We want to thank the following people, for help, input, quotes, experience and time.

- Riona
- Margaret Eker
- Jennifer Rondeau
- Sarah
- Eric Holscher
- Sarah Karp
- Kate Kiefer Lee
- Istvan Zoltan Szabo
- Tom Johnson
- Write The Docs Community
