========
Workflow
========

.. topic:: Remember

    The hardest thing is to actually use and follow a procedure.


Choose A Pattern
================

- Document/Blueprint functionality
- Code [with taking notes]
- Test Code
- Transform your notes into documentation
- Test documentation
- Release

Define Procedure
================

- Document/Blueprint functionality: You're giving yourself a chance to think through the project without having to change code every time you change.
- Code: Split it in smaller parts, like one function, try to keep your docs in sync.
- Test Code: Test your code
- Transform your notes into documentation: Tranform your notes into documentation, following your style-guide and company strategy.
- Test documentation: Test your newly written docs, against your documentation tests
- Release: If **all** the tests [code and docs] are green: Release, otherwise fix.
