==============
Basic Testing
==============

Checklist
=========

* Audience: E.g., make sure documentation is not too novice or too advanced.
* Terminology: Is it suitable for the audience?

    * Terms used consistently?
    * Abbreviations for acronyms?

* Content and subject matter:

    * Appropriate subjects covered?
    * No subjects missing?
    * Proper depth?
    * Missing features described accidentally?

Human-Interface-Test
====================

The :term:`HIT` [Human-Interface-Test] is the **most** important test.

Let user test your documentation, choose user according to the addressed audience of the documentation and let them test.

Analytics
=========

* Make sure user find what they are looking for.

Spell-Check
===========

* Directly in editor, as it often involves choosing an alternative.
* As a linter in automated tests.

Link-Check
==========

* If you work on a document, always make sure that all links are working, it's easier and faster to check one file, than the whole docs.

Consistency
===========

* Always the same term, GitHub not github or Github.

Writing
=======

* Follow your style-guide.
* Catch insensitive, inconsiderate writing.
* Suggest shorter sentences.
* Avoid common pitfalls in grammar and style.
