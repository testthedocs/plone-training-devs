==============
Visual Testing
==============

Checking The Look And Feel
==========================

There are different *levels* of visual testing.

* Is my syntax OK ?
* How does my docs looks like in HTML ?


Getting A Visual Impression
===========================

Use A Editor With Preview
-------------------------

Sublime Text 3 with Omni-preview


.. image:: ../_static/mr.otlet_sublime-omi.gif
   :alt: Picture of a open sublime using a preview add-on


Vim using `InstantRST <https://github.com/Rykka/InstantRst>`_

.. image:: ../_static/rst-vim-pre.gif
   :alt: Picture of a open vim using a preview add-on


Online reStructuredText Editor

.. image:: ../_static/online_rst_editor.png
   :alt: Picture of http://http://rst.ninjs.org/ converting rst to HTML.



Medium
------

* `mr.docs <https://github.com/tiramisusolutions/mr.docs>`_, a tool written for writing and testing Plone documentation.


Advanced
--------

* Sphinx
* Pandoc locally installed
