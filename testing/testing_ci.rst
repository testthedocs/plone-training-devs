===============
Testing with CI
===============

Use continuous integration to test your documentation.

Example
=======

This example shows a Travis configuration file [*.travis.yaml*].

.. code-block:: yaml

    sudo: required

    services:
      - docker

    before_install:
      - docker pull testthedocs/ttd.docsbuilder

    script:
      - docker run -it --rm -v "${PWD}":/build/docs:rw -u "$(id -u)":"$(id -g)" testthedocs/ttd.docsbuilder testbuild

We use Docker on Travis to run a container called *testthedocs/ttd.docsbuilder* to run a nit-picky build of the docs.
