========================
Test Against An Audience
========================

Before publishing, do a :term:`HIT` [Human-Interface-Test], make sure that your docs reach the audience.

* Almost no developer can push out their code without running it through QA, the same applies to documentation.
* Users can’t overlook instructions that don’t work, that don’t speak to the real steps and challenges they face.
* Users will tell you if your instructions are missing a step, screen-shots or logic.
