==========
Exercise 2
==========

In this exercise we will use a Git pre-commit hook, to ensure our docs follow certain requirements.

For that we will work in the same example.

Creating A Hook
===============

Change to the `hooks` directory of `.git`

.. code-block:: bash

  cd .git/hooks

Copy the following script as `pr-commit` in this directory

.. code-block:: bash

      #!/bin/sh
    # pre-commit script to run some tests against the base training docs

    # Colors
    COL_RESET=$ESC_SEQ"39;49;00m"
    COL_YELLOW=$ESC_SEQ"33;01m"

    # Sphinx in nit-picky mode, doing a HTML test build.
    echo -en "$COL_YELLOW Running HTML test build$COL_RESET\n"
    docker run --rm -v "${PWD}":/build/docs:rw -u "$(id -u)":"$(id -g)" \
        testthedocs/ttd.docsbuilder testbuild

    # Sphinx running spell-check, this is used, till we fix the SpellCheckBear.
    echo -en "$COL_YELLOW Running Sphinx spell-check$COL_RESET\n"
    docker run --rm -v "${PWD}":/build/docs:rw -u "$(id -u)":"$(id -g)" \
        quay.io/tiramisu/mr.docs spellcheck

    # Unleash some bears to do some base testing against DOCS.
    # Bears we use:
    # SpaceConsistencyBear, LineLengthBear, InvalidLinkBear, Write-Good-Bear, Language-Tool-Bear,
    # MakefileBear, Git-Commit-Bear
    echo -en "$COL_YELLOW Running bears$COL_RESET\n"
    coala-ci DOCS
    exit 0

Make it executable

.. code-block:: bash

  chmod +x pre-commit

Change back into the top of the directory

.. code-block:: bash

  cd ../..

Task
====

- Work further on the docs of your Plone add-on, add a correct and working link and add a wrong and not working link.
- Add some spaces.

When you are done, do a `git commit`

.. code-block:: bash

  git commit
