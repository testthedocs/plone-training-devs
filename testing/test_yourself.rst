=============
Test Yourself
=============

While testing your documentation, you must recognize that what may seem clear to you may be confusing to another, because all documentation builds on assumptions that may or may not be shared with your audience.

For example, you may assume that users already know how to use buildout, pin versions and install an add-on.

Usually documentation doesn’t hold a user’s hand from beginning to end, but rather jumps into a specific task that depends on concepts and techniques that you assume the user already knows.

With developer documentation, usually the audience’s skill level is far beyond my own.
Adding little notes that clarify obvious instruction (such as saying that the *$* in code samples signals a command prompt and shouldn’t be typed in the actual command) isn’t essential.
Adding these notes can’t hurt, especially when some users of the documentation are product marketers rather than developers.
