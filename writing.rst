=======
Writing
=======

Language is the most powerful tool natural selection has ever devised.

Entropy In Communication
========================

* Obscurity , Uncertainty, Ambiguity

  * Constantly working against the transmission of information

* In speech we use redundancy, but this is unnecessary in writing
* English lacks preciseness other languages have

How Can We Stretch The Borders?
===============================

Follow A Process
----------------

* Write - Create structure, estimate word count, focus on flow.
* Let the draft rest - switch your mind to editing mode
* Editing - Self edit, check grammar (with grammar checkers), read out loud, send to editor

Ways To Stretch The Borders Of Your Language
--------------------------------------------
* Translating a book (nonfiction is easier)
* Watch a series - shows the most recent state of language, teaches pop culture (which finds its way into very different context!)
* Get a mentor - not a teacher, but a coach. Their advice is more strategic than technical

  * This is difficult - but they come from personal or professional connections and there’s no shortcut
  * If no mentor - Get an Amazon account [READ], read blogs, go to events and meetups

* Practice
  * Keep a daily journal (helps to articulate thoughts quickly and effective, and you can track your progress)

  * Start a blog or Medium account
  * Create a work-flow to stay on the path
  * Give a presentation



