===========
Screencasts
===========

Screencasts (video or animated images) can be a powerful means to document your software.

They can serve a dual purpose as well, to both serve as "demonstration" and as actual documentation.

However, there are also drawbacks.

Long-form Video
===============

In this setting, long-form can be anything from 2 minutes to half an hour or more.
These kind of videos are best used as 'demonstration'-type, which means you will want to spend some time preparing them.

- Create a script with both what you want to show (visual) and what you want to tell (audio)
- Make sure your setup is working: a good microphone/headset, post-processing software
- Do test runs, until you can go through the script fluidly
- Speak slow, be deliberate and articulate
- Use a neutral voice and 'international English', avoid country-specific idiom as far as possible
- Make sure your mouse pointer is visible. Many operating systems have built-in ways to make it bigger and/or have a 'glow'

Long-form video will almost always need post-processing.
It needs an introduction screen, and usually closing credits with further links.

Do remember that 85% of videos these days are viewed **without sound**!!
That means you will want to have captions or text-overlays with the text you are speaking.
(captions are better for accessibility, text-overlays can look nicer)

Very useful is Youtube's "auto-caption" facility: you can upload a video, and have Youtube do a rough automated voice-recognition and captioning.
You will most likely need to improve, but it is a huge time-saver.


Overall, long-form videos require a considerable time-investment, and are very hard to keep up to date.

As pure documentation, that is probably not worth it. As combined marketing and documentation, it can be an option.


Animated Screenshots And Short Videos
=====================================

These are a much more viable option.

Some can be scripted, and the output can be in the form of an animated gif.
Animated gifs have the big advantage they can be stored directly in your docs; videos are usually outsourced on Youtube or Vimeo, which leads to more distraction.
