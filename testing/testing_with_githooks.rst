======================
Testing with Git Hooks
======================

Use Git Hooks to run tests locally before you commit your changes.

Advantages
==========

- Git Hooks are a great way to ensure the quality standards of your docs.
- Git Hooks can help to divide and split your tests.
- Git Hooks can help you to speed up your tests.

Examples
--------

Pre-commit Hook


.. raw:: html

    <script type="text/javascript" src="https://asciinema.org/a/88561.js" id="asciicast-88561" async></script>

.. code-block:: bash

    #!/bin/sh
    # pre-commit script to run some tests against the base training docs

    # Colors
    COL_RESET=$ESC_SEQ"39;49;00m"
    COL_YELLOW=$ESC_SEQ"33;01m"

    # Sphinx in nit-picky mode, doing a HTML test build.
    echo -en "$COL_YELLOW Running HTML test build$COL_RESET\n"
    docker run --rm -v "${PWD}":/build/docs:rw -u "$(id -u)":"$(id -g)" \
        testthedocs/ttd.docsbuilder testbuild

    # Sphinx running spell-check, this is used, till we fix the SpellCheckBear.
    echo -en "$COL_YELLOW Running Sphinx spell-check$COL_RESET\n"
    docker run --rm -v "${PWD}":/build/docs:rw -u "$(id -u)":"$(id -g)" \
        quay.io/tiramisu/mr.docs spellcheck

    # Unleash some bears to do some base testing against DOCS.
    # Bears we use:
    # SpaceConsistencyBear, LineLengthBear, InvalidLinkBear, Write-Good-Bear, Language-Tool-Bear,
    # MakefileBear, Git-Commit-Bear
    echo -en "$COL_YELLOW Running bears$COL_RESET\n"
    coala-ci DOCS
    exit 0

This pre-commit runs serval checks against the docs:

- Nit-picky HTML test build
- Sphinx-spell-check
- Line-length check
- Link-check
- Wording check


