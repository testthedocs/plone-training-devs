=======
Editors
=======

ReText
======

Simple but powerful editor for Markdown and reStructuredText.

.. image:: ../_static/mr.otlet_retext.png
   :align: center
   :alt: Picture of a open ReText editor

`ReText Website <https://github.com/retext-project/retext>`_


Sublime Text 3
==============

`Sublime Text 3 <https://www.sublimetext.com/3>`_ is a sophisticated text editor for code, markup and prose.

.. image:: ../_static/mr.otlet_sublime-omi.gif
   :align: center
   :alt: Picture of a open sublime using a preview add-on


Add-ons
-------

Easy
~~~~

* reStructuredText-linter
* omni-preview
* word-count
* language-tool
* git-gutter


Advanced
~~~~~~~~

* write-good
* coala
* builder-plugins

Settings
--------

.. code-block:: bash

    {
    "added_words":
    [
        "Plone",
        "online",
        "Zope",
        "GitHub"
    ],
    "draw_white_space": "all",
    "enable_live_count": true,
    "enable_readtime": true,
    "ensure_newline_at_eof_on_save": true,
    "show_encoding": true,
    "show_line_endings": true,
    "spell_check": true,
    "tab_size": 4,
    "translate_tabs_to_spaces": true,
    "trim_automatic_white_space": true,
    "trim_trailing_white_space_on_save": true


