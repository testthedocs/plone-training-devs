============
Using Docker
============

Here we use Docker to do a test build of our documentation in `nit-picky` mode, which means warnings will treated as errors and the build will stop and exit on the first error.

Example
=======

.. raw:: html

    <script type="text/javascript" src="https://asciinema.org/a/88548.js" id="asciicast-88548" async></script>

