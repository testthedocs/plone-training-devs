======
Linter
======

.. note::

    Linter and analyzer are helpful if there are setup right, doing it wrong can lead to wrong results and a decreasing quality of your documentation.


As there are linter for testing different programming languages there are also linter for testing grammar, wording, spelling and reStructuredText.

Be careful ! Linter are helpful if there are setup right, doing it wrong can lead to wrong results and a decreasing quality of your documentation.


Example Linter
==============

Write Good
----------

`Naive linter for English prose for developers <https://github.com/btford/write-good>`_

.. raw:: html

    <script type="text/javascript" src="https://asciinema.org/a/88040.js" id="asciicast-88040" alt="Short video example" async></script>


Analyzer
========

Use a code analyzer to test your documentation.

To unify tests we can use `coala (unified code analyzer) <http://coala.io/>`_.

Advantage
---------

* One configuration file for tests
* Comes with 'batteries' included:

    * Spell-check
    * Link-check
    * Write-good
    * Language tool interrogation
    * Line-length check
    * Many more

Example
~~~~~~~

.. raw:: html

    <script type="text/javascript" src="https://asciinema.org/a/88047.js" id="asciicast-88047" async></script>
