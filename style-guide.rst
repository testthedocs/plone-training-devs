===========
Style Guide
===========

**Wikipedia**

A style guide (or manual of style) is a set of standards for the writing and design of documents.
Either for general use or for a specific publication, organization, or field. (It is often called a style sheet, though that term has other meanings.)

A style guide establishes and enforces style to improve communication.
To do that, it ensures consistency within a document and across multiple documents and enforces best practice in usage and in language composition, visual composition, orthography and typography.
For academic and technical documents, a guide may also enforce the best practice in ethics (such as authorship, research ethics, and disclosure), pedagogy (such as exposition and clarity), and compliance (technical and regulatory).

Style guides are common for general and specialized use, for the general reading and writing audience, and for students and scholars of various academic disciplines, medicine, journalism, the law, government, business, and specific industries.


Style Guide For Documentation
-----------------------------

So, why does having (or adhering to an already existing) style guide help documentation?

-   Documentation is "prose for a purpose".
    You do not write documentation as a literary exercise, like you would write a novel or a poem.
    You write it so people understand how to use a product or feature.
    Readers want to focus on the task at hand; a consistent use of language throughout the documentation lowers the cognitive load.

-   Documentation is usually written by several people.
    A style guide allows the writers to all write in similar tone and voice.
    This helps the user, who is not so much interested in the personality of the documentation author,
    but it also helps the writer to stay neutral, convey her or his meaning more efficient, and be a better documentation writer.

-   Style guides help both writers and readers for who English is not their first language.
    And even if English is your first language, it is also one of the most free-form languages available.
    There are many ways to state the same thing, all of which are completely valid in (one or the other) form of English.
    Which is good, especially in literature and poetry.
    But it can be confusing or at least distracting in a technical manual.

-   If a company or community has decided on a particular :doc:`tone and voice <tone>` the style guide keeps that consistent.

-   Consistency for the reader: a pop-up message is always a pop-up message, not sometimes a pop-up box or a pop-up question (unless there is a **functional** difference!)

-   It helps avoid jargon.
    Everybody in your company or community may know what "Through The Web" or "TTW" is.
    But the rest of the world doesn't.
    Those are the people that need your documentation.
    The style guide can help you avoid that group-specific language by making explicit how to avoid it.
