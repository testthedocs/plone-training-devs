=========
Exercises
=========

.. toctree::
   :hidden:
   :maxdepth: 2

   exercise1
   exercise2

Please make sure that you have the following installed:

- Python 3
- VirtualEnv
- Node/NPM
- Git
