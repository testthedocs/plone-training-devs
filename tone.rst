====
Tone
====

**“Creating meaningful interactions with users requires a strategic voice with a human tone.”**

Voice = personality

Tone = mood

Great example: MailChimp - the voice stays the same, but tone changes throughout docs.

Why is Voice and Tone so difficult?
- It changes across scenarios (billing, using the product, etc).
- Need to interpret how customers are feeling across each scenario.
- Tone has a personal preference.

You’ve Got The Power
====================

- Understand and utilize the full power. Everyone needs to know why it’s helpful.
- Brand identity
- Human connection
- Customer engagement

Define Your Own Voice
=====================

- Look through your company values, and see what stands out that should be in your content too.
- Bold, Optimistic, Practical, with a wink.
- All tone guidelines have an opposite too (eg. Bold, but not aggressive).


Voice Doesn’t Change
====================

- Document doesn’t need to be identical, but it should stay consistent.

**Voice and tone builds trust - people know you and can identify you!**
