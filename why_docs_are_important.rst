==============================
Why Documentation Is Important
==============================

**"Documentation is product. You can't have one without the other"**

For Yourself
============
- You're giving yourself a chance to think through the project without having to change code every time you change your mind or add a feature.

Your Project
============
- It's much easier to write documentation at the beginning of the project when your excitement and motivation are at their highest.

- Retroactively writing documentation is much harder, and you're sure to miss all kinds of important details when you do so.

Your Business
=============
- If everyone else on the team has access to this information before you've completed the project, then they can confidently start work on other projects that will interface with your code.
  Without any sort of defined interface, you have to code in serial or face reimplementing large portions of code.

- Good documentation gets people to jump into your project much, much quicker.

- Good documentation can save an organization time, efforts and money.

- If you don't have an absolute clarity in the code you're pushing, it rears its head by way of bugs, confused coworkers, and slow code. Straightforward documentation gives you that clarity.

Community
=========
- It's a lot simpler to have a discussion based on something written down. It's easy to talk endlessly and in circles about a problem if nothing is ever put to text.

- The simple act of writing down a proposed solution means everyone has a concrete idea that can be argued about and iterated upon.

- Documentation means others can read your project much, much quicker.

- Different people have different skills. Adding to documentation is a good way for end-users and others to contribute.

User Satisfaction
=================
- If the user is not satisfied, how we are going to make money out of that product?

- Lack of documentation *is* a problem for acceptance.
