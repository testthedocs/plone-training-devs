==========
Exercise 1
==========

In this exercise we will practice the usage of `coala <https://coala.io/>`_ for running basic checks against documentation.

For that we will use a example repository, which already comes with a example of a coala configuration file.

Please follow the instructions bellow in order to get started.

Setup
=====

Cloning the example repository:

.. code-block:: bash

    https://github.com/testthedocs/ttd.docs-example.git

Change into `ttd.docs-example`

.. code-block:: bash

    cd ttd.docs-example

Create VirtualEnv:

.. code-block:: bash

    virtualenv .

Activate VirtualEnv:

.. code-block:: bash

    source bin/activate

Install coala with PIP

.. code-block:: bash

    pip3 install coala-bears --pre

Task
====

Imagine a Plone add-on which is a feed reader.
It downloads news feeds published by web sites and aggregates their content together into a single combined feed, latest news first.

Audience: 'Through the Web' Integrators, who are able to run buildout.

- Create documentation structure for the following:

  * About
  * Features
  * Install
  * De-install

- Use the example README as a starting point

During writing keep in mind that you always work with a :doc:`strategy <../strategy>`.

Testing your docs
=================

- Check your docs for spelling mistakes
- Partner up with the person next to you and let this person proof read your docs.
- Change to the top of the repository and run coala against the docs.

.. code-block:: bash

    coala-ci DOCS
