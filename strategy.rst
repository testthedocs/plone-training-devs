========
Strategy
========

How to structure documentation, how to start out, and how to ensure quality and enhance


How To Structure
================

Think In Tasks
--------------

Almost nobody will read your documentation in one go.
People will read the part that is relevant to the task at hand.
Then they will return to another part when they need that.

So, design your documentation to be read in parts.

One of those tasks is "I want to get an overview", so there definitely should be an overall introduction to what your software does!

But the details of the tasks should be in separate chapters, that people can look up when they need it.

Thinking in tasks gives you a good first structure:

- One task, one chapter.
- Do not assume people have read the previous chapter.
  They may have, but maybe it was a long time ago.
  If it is necessary that they have read another chapter, make that explicit by linking to it.
- If a task is not logically finished by the end of your chapter, make that explicit as well. "Now, you can go and add tags to the image you just created", where 'add tags' is a link to the chapter titled 'Adding tags'.

Group By Audience / Role
------------------------

- "Not a user yet, just checking out the software" **is** an audience, or a role that most of your users will start out as!
- Group tasks that have a logical connection. If you create images, you probably want to move, tag, update and delete them as well.
- Be specific, and separate out, if a task needs special rights or knowledge. "To set up the vocabulary, you will need site administrator rights"
- Separate tasks that occur once or seldom (installation, de-installation) from day-to-day tasks, and do not 'force' users to go through a lengthy installation manual by putting it at the start. It's OK to move this to an appendix.
- If a task, or set of tasks, cannot be left at an in-between state or incomplete, make that explicit and indicate the time it will take to go through the whole task.
- Specify requirements that users will need while in this role."Before you install,make sure you have the following information: A fully qualified domain name, an SSL certificate, the address of your mail service." (with links to a glossary or further documentation to define those, if needed)


Think Of How A User Gets To A Specific Section
----------------------------------------------

- Create a clear landing page, leading a user to the part she needs *at that time* in the quickest way possible.
- However, many users will also arrive at a specific part of your documentation by using a search engine, and thus not see your landing page.
  A clear title and description will help those users determine if they followed the right link.
- When people *most* need your documentation is when something breaks for them. Be aware of that. Offer solutions to fix things when somebody is googling for the name of your software with "doesn't work".


Grow Your Documentation
=======================

No documentation will likely be perfect from the start.
Almost like code, isn't it?

If, in the beginning, this all seems overwhelming, remember the 80/100% rule.
It is better to have 80% good, and up-to-date, documentation, than to try to reach 100% completeness.
It will probably be out of date and partly not working anymore by the time you reach 100%

Just like for code: release early, release often!

Keep Your Docs Close
--------------------

Documentation should live close to the code, ideally in a /docs directory.
That way, developers can write a first draft, even if it may not be perfect.

The Plone documentation does not follow this ideal example, because of historical reasons, and because of the large number of sub-packages that constitute "Plone the product".
It could be put under Products.CMFPlone but that would be just as wrong, and harder to find for contributors.
So, in this case, usability (and a long and convoluted history) wins over strict guidelines.


Release Planning
----------------

- Scope - is it an update? A full feature release? A new product?
- Design - what are your deliverables? How will it be deployed?
- Development - Write, Review, Test (just like code!)

Docs as code makes sense, because if you’re all working in the same environment, developers can literally write the first draft.


Structural And Functional Quality
=================================

Structural Quality
------------------
- Are the spelling and grammar correct?
- Style and usage guidelines
- Does it use proper voice and tone?
- Is it well-organized?
- Is it easy to navigate?

Structural quality is “pretty” but it doesn’t measure effectiveness.

Functional Quality
------------------

- Does it do what it’s supposed to do?
- Does it satisfy requirements?
- Achieve what it sets out to achieve for users?

E.g. does it actually promote using a new feature or product?

**High structural quality + Low functional quality = POOR overall quality**

Functional Quality is **ALWAYS** the most important

Mindset
-------

- Understand
- Define
- Execute
- Measure

1.Start with functional requirements
- State what the document should achieve or what it’s supposed to DO

2.Define functional requirements with internal and external stakeholders
- Eg. deflect support calls, increase sign-ups
- You can have all sorts of requirements, but the important thing is to acknowledge the

3.Execute on the docs!

4.Measure success
- To demonstrate value we need to focus on functional quality data
- Structural data is unpersuasive - “uses active voice 85% of the time” - meh.
- Sentiment data - are the stakeholders (including users) are happy with docs

5.Use support data, ask users, etc
- Use quality data to communicate impact and value
- Talking about impact with other teams
- Product teams and managers

  * Eg. “We wanted to increase feature adoption by 10% in Q4”
  * I restructured the docs
  * I worked with marketing for messaging to drive users to the docs
  * Follow up study

Writing
=======

- When to write
- Which order
- How much
- Who is my audience

See further in :doc:`writing <writing>`.

Testing
=======

- Test as much as can be tested by automation, as it will give you time to focus on the parts that need humans
- What to test
- When to do certain tests

.. note:: You should start testing your documentation from the moment you start writing it !
    If you wait too long, it will cost a lot of time, energy and money to get it right again.
