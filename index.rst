========================
Docs Or It Didn't Happen
========================

**Documentation is a tool to achieve the goal, and the goal is working software.**

.. toctree::
    :hidden:

    why_docs_are_important
    philosophy
    strategy
    style-guide
    tone
    writing
    workflow
    screenshots
    screencasts
    project_structure
    tools/editors
    testing/index
    exercises/index
    credits
    glossary

.. image:: _static/write-drunk-edit-sober.png
   :alt: Picture 'Write drunk, edit sober'

