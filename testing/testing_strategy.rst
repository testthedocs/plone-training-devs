================
Testing Strategy
================

**Testing Documentation is art**

Adjust your tests according to your audience and goal.

Not all the tests are always equally important.

Keep In Mind
============

* Use tests from the beginning
* Start with easy tests
* Do not try to test too much, too strict in the beginning [depends on writer and audience]
* Increase slowly and steady your amount of tests
* Tests your tests, make sure that they are working
* How to test: Locally, CI or both

Consideration
=============

If you work on the docs of a community project you need to adjust your testing strategy.

- Not everyone is a native speaker of the language your docs are written in
- Different levels of language skills
- Too restricted tests will scare people
- You want to motivate people
- Split testing level between local and CI tests [strict on local tests, less strict on CI tests]
- Increase tests and strictness with time



