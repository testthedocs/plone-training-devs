============
Screenshots
============

How many screenshots should I include?

Screenshots are only important to documentation if they add value to the material.
They can be very helpful when explaining in words takes much more time.
However, do keep accessibility in mind: A screenshot should not be the **only** source of information.

Illustrating Hard To Explain Concepts
=====================================

If your interface is radically different from what a user might be used to (or if it's too cluttered or otherwise not great), that's a good indication that you should include some screen shots.
If you can't find the words to clearly explain what you want the reader to do, a screen shot may be a good band-aid while you improve the interface.
The key here is what you want the reader to do. Generally, you don't need to include screen shots for UI elements that the user doesn't really interact with.

What's Your Audience's Skill Level?
===================================

Of course, the audience is an important consideration. If the reader has used a GUI-based desktop before, you don't need to explain the "File -> Print" menu.
If you're writing for complete neophytes, then maybe it's a good idea. Similarly, if the reader is an experienced user, you don't need to screenshot each pop-up dialog.
"Error: file not found" and "Error: could not write to file" are not dissimilar enough to require a graphical explanation, especially if the only action for both is to click the the "OK" button.

Rule Of Thumb
=============

- Add screenshots into your Style Guides

  * What areas to snapshot
  * What format to save in
  * Post processing - what tools to use?
- Create a cheat sheet

**Screen-shots are most effective when they are meant to serve a specific purpose**
